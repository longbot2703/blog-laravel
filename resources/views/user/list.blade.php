@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>User Manager</h1>
        @if (session('success'))
        <div class="flash-message">
            <p class="alert alert-success">{{Session('success')}}</p>
        </div>
        @endif

        <div class="row mt-5" style="margin-top:20px;">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                    <tr style="background-color:rgb(215, 243, 227)">
                        <th class="text-center">No.</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Role</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lsUser as $user)
                        <tr class="text-center">
                            <td>{{$user->id}}</td>
                            <td class="name">{{$user->name}}</td>
                            <td class="role">{{$user->role->name}}</td>
                            <td>
                                <a href="{{route('user_management.edit', $user->id)}}" <i
                                    class="fa fa-pencil-square-o text-success"></i></a>
                                <a onclick="delUser('{{$user->id}}')"><i class="fa fa-trash text-danger"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            @endsection

            @section('script')

                <script type="text/javascript" src="{{asset('js/user.js') }}"></script>
@endsection
