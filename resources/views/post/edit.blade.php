@extends('layouts.app')

@section('content')
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <div class="container">

        @if(count($errors) > 0)
            <div class='alert alert-danger'>
                @foreach($errors->all() as $er)
                    <p>{{$er}}</p>
                @endforeach
            </div>
        @endif

        <form method="post" action="{{route('post_management.update', $post->id)}}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <input type="hidden" name="id" value="{{$post->id}}">
            <div class="form-group">
                <label for="name">Cover Image</label>
                <input type="file" class="form-control" id="file" name="file" placeholder="Enter Title">
            </div>

            <div class="form-group">
                <label for="name">Title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title"
                       value="{{$post->title}}">
            </div>

            <div class="form-group">
                <label for="name">Summary</label>
                <input type="text" class="form-control" id="summary" name="summary" placeholder="Enter summary"
                       value="{{$post->summary}}">
            </div>

            <div class="form-group">
                <label for="name">Content</label>
                <textarea rows="6" cols="150" name="content">{{$post->content}}</textarea>
            </div>

            <div class="form-group">
                <label for="name">Category</label>
                <select name="category" class="form-control">
                    @foreach($Category as $cate)
                        <option value="{{$cate->id}}"
                            {{$post->category_id == $cate->id ? 'selected' : ''}}
                        >{{$cate->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="name">Active</label>
                <select name="is_active" class="form-control">
                    <option value="0" {{$post->is_active == 0 ? 'selected' : ''}}>Draft</option>
                    <option value="1" {{$post->is_active == 1 ? 'selected' : ''}}>Publish</option>
                </select>
            </div>
            <div class="field form-group">
                <label for="name" class="label">Tag</label>
                <select name="tags[]" multiple class="form-control">
                    @foreach($Tag as $tag)
                        <option
                            value="{{$tag->id}}" {{$post->tag_id == $tag->id ? 'selected' : ''}}>{{$tag->name}}</option>
                    @endforeach
                </select>
            </div>


            <button type="submit" class="btn btn-primary">Submit</button>

        </form>
    </div>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection


