@extends('layouts.app')

@section('content')
    <div class="container">

        @if(count($errors) > 0)
            <div class='alert alert-danger'>
                @foreach($errors->all() as $er)
                    <p>{{$er}}</p>
                @endforeach
            </div>
        @endif

        <form method="post" action="{{route('post_management.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Cover Image</label>
                <input type="file" class="form-control" id="file" name="file">
            </div>

            <div class="form-group">
                <label for="name">Title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
            </div>

            <div class="form-group">
                <label for="name">Summary</label>
                <input type="text" class="form-control" id="summary" name="summary" placeholder="Enter summary">
            </div>

            <div class="form-group">
                <label for="name">Content</label>
                <textarea rows="6" cols="150" name="content"></textarea>
            </div>

{{--            <div class="form-group">--}}
{{--                <label for="name">Category</label>--}}
{{--                <select name="category_id" class="form-control">--}}
{{--                    @foreach($Category as $cate)--}}
{{--                        <option value="{{$cate->id}}">{{$cate->name}}</option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
{{--            </div>--}}

            <div class="form-group">
                <label for="name">Active</label>
                <select name="is_active" class="form-control">
                    <option value="0">Draft</option>
                    <option value="1">Publish</option>
                </select>
            </div>

{{--            <div class="field form-group">--}}
{{--                <label for="name" class="label">Tag</label>--}}
{{--                <select name="tags[]" multiple class="form-control">--}}
{{--                    @foreach($Tag as $tag)--}}
{{--                        <option value="{{$tag->id}}">{{$tag->name}}</option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
{{--            </div>--}}

            <button type="submit" class="btn btn-primary">Submit</button>

        </form>
    </div>

    <script>
        CKEDITOR.replace('content');
    </script>


@endsection


