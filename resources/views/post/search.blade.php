@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Post Manager</h1>

        <div class="flash-message">
            <p class="alert alert-success">{{Session::get('success')}}</p>
        </div>

        <div style="font-size: 20pt"><a href="{{route('post_management.create')}}">Add new</a></div>

        <form action="{{url('/search')}}" role="search" method="get" id="searchform">
            <div class="row">
                <div class="col-sm-9 col-md-9 col-lg-9 col-xl-9">
                    <input type="text" id="post-title" name="key" class="form-control" placeholder="Enter title"/>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right">
                    <button class="btn btn-success" type="submit" id="btnSearchPost">
                        <i class="fa fa-search mr-1"></i>Search
                    </button>
                </div>
            </div>
        </form>

        <div class="row mt-5" style="margin-top:20px;">
            <div class="col-md-12" id="postTable">
                <table class="table table-bordered">
                    <thead>
                    <tr style="background-color:rgb(215, 243, 227)" class="text-center">
                        <th>No.</th>
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Tag</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($post as $p)
                        <tr class="text-center">
                            <td class="">{{$p->id}}</td>
                            <td class="title">{{$p->title}}</td>
                            <td class="summary">{{$p->summary}}</td>
                            <td class="category">{{optional($p->category)->name ?? ''}}</td>
                            <td class="status">{{$p->is_active == 1 ? 'Publish' : 'Draft'}}</td>
                            <td class="tag">
                                @foreach($p->tags as $tag)
                                    {{$tag->name}}
                                @endforeach
                            </td>

                            <td class="text-center">
                                <a href="{{route('post_management.edit', $p->id)}}"
                                <i class="fa fa-pencil-square-o text-success"></i></a>
                                <a onclick="delPost('{{$p->id}}')"><i class="fa fa-trash text-danger"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endsection

            @section('script')

                <script type="text/javascript" src="{{asset('js/post.js') }}"></script>
@endsection
