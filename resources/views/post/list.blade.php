@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Post Manager</h1>

        @if (session('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{Session('success')}}</p>
            </div>
        @endif

        <div style="font-size: 20pt"><a href="{{route('post_management.create')}}">Add new</a></div>

        <form action="{{url('/search')}}" role="search" method="get" id="searchform">
            <div class="row">
                <div class="col-sm-9 col-md-9 col-lg-9 col-xl-9">
                    <input type="text" id="post-title" name="key" class="form-control" placeholder="Enter title"/>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right">
                    <button class="btn btn-success" type="submit" id="btnSearchPost">
                        <i class="fa fa-search mr-1"></i>Search
                    </button>
                </div>
            </div>
        </form>

        <div class="row mt-5" style="margin-top:20px;">
            <div class="col-md-12" id="postTable">
                <table class="table table-bordered">
                    <thead>
                    <tr style="background-color:rgb(215, 243, 227)" class="text-center">
                        <th>No.</th>
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Tag</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($Post as $post)
                        <tr class="text-center">
                            <td class="">{{$post->id}}</td>
                            <td class="title">{{$post->title}}</td>
                            <td class="summary">{{$post->summary}}</td>
                            <td class="category">{{optional($post->category)->name ?? ''}}</td>
                            <td class="status">{{$post->is_active == 1 ? 'Publish' : 'Draft'}}</td>
{{--                            <td class="tag">--}}
{{--                                @foreach($post->tags as $tag)--}}
{{--                                    {{$tag->name}}--}}
{{--                                @endforeach--}}
{{--                            </td>--}}

                            <td class="text-center">
                                <a href="{{route('post_management.edit', $post->id)}}">
                                <i class="fa fa-pencil-square-o text-success"></i></a>
                                <a onclick="delPost('{{$post->id}}')"><i class="fa fa-trash text-danger"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
{{--                {{$Post->links()}}--}}
            </div>
            @endsection

            @section('script')

                <script type="text/javascript" src="{{asset('js/post.js') }}"></script>
@endsection

