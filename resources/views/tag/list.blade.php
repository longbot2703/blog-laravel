@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Tag Manager</h1>
        @if (session('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{Session('success')}}</p>
            </div>
        @endif
        <div style="font-size: 20pt"><a href="{{route('tag_management.create')}}">Add new</a></div>

        <div class="row mt-5" style="margin-top:20px;">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                    <tr style="background-color:rgb(215, 243, 227)">
                        <th class="text-center">No.</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($allTag as $tag)
                        <tr>
                            <td class="text-center">{{$tag->id}}</td>
                            <td class="name text-center">{{$tag->name}}</td>
                            <td class="text-center">
                                <a href="{{route('tag_management.edit', $tag->id)}}" <i
                                    class="fa fa-pencil-square-o text-success"></i></a>
                                <a onclick="delTag('{{$tag->id}}')"><i class="fa fa-trash text-danger"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


            @endsection

            @section('script')

                <script type="text/javascript" src="{{asset('js/tag.js') }}"></script>
@endsection
