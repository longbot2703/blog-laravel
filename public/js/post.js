
function delPost(id) {
    swal({
        title: "Are you sure?",
        text: "",
        icon: "warning",
        buttons: ['Cancel', 'OK']
    }).then((sure) => {
        if (sure) {
            callAjax('post_management/destroy', {id: id}, 'delete')
                .then(function (res) {
                    if (res.status == 1) {
                        swal({
                            title: res.message, text: "", icon: "success"
                        }).then((success) => {
                            location.reload()
                        })
                    } else {
                        swal({
                            title: res.message, text: "", icon: "error",
                        })
                    }
                })
                .catch(function (error) {

                })
        }
    })
}


function searchPost() {

    var title = $.trim($('#title').val());
    $('#modalLoad').modal('show');
    callAjax('posts.search', {id: id}, 'get')
        .then(function (res) {
                $('#modalLoad').modal('hide');
                $('#postTable').html(res);
            })
        .catch(function (error) {

        })
}

function callAjax(url = '', data = [], method = 'get') {
    return $.ajax({
        method: method,
        url: url,
        data: data
    });
}
