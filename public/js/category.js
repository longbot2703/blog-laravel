function delCate(id) {
    swal({
        title: "Are you sure?",
        text: "",
        icon: "warning",
        buttons: ['Cancel', 'OK']
    }).then((sure) => {
        if (sure) {
            callAjax('cate_management/destroy', {id: id}, 'delete')
                .then(function (res) {
                    if (res.status == 1) {
                        swal({
                            title: res.message, text: "", icon: "success"
                        }).then((success) => {
                            location.reload()
                        })
                    } else {
                        swal({
                            title: res.message, text: "", icon: "error",
                        })
                    }
                })
                .catch(function (error){

                })
        }
    })
}

