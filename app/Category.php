<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Requests\CategoryRequest;

class Category extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }

    public function findBy($id)
    {
        return $this->where('active',   1)->where('publish',   1)->find($id);
    }

    public function getAll()
    {
        return $this->all();
    }
}
