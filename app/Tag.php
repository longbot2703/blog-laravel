<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\TagRequest;

class Tag extends Model
{
    public function posts()
    {
        return $this->belongsToMany(Post::class,'post_tag','tag_id','post_id');
    }

    public static function createTag(TagRequest $request)
    {
        $tag = new Tag();
        $tag->name = $request->name;
        $tag->created_at = Carbon::now();
        $tag->save();

        return $tag;
    }

    public static function updateTag(TagRequest $request, $id)
    {
        $tag = Tag::find($id);
        $tag->name = $request->name;
        $tag->save();

        return $tag;
    }
}
