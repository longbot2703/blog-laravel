<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\PostRequest;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'title',
        'summary',
        'content',
        'category_id',
        'user_id',
        'is_active',
        'cover',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tag', 'post_id', 'tag_id');
    }

    public function findBy($id)
    {
        return $this->find($id);
    }

    public function getAll()
    {
        return $this->all();
    }

    public function getPaginate($perPage)
    {
        return $this->paginate($perPage);
    }

    public static function createPost(PostRequest $request)
    {
        $data = $request->all();
//        $data['cover'] = self::uploadFile($request->file('file'));
//        $data['user_id'] = auth()->check() ? auth()->user()->id : '';

        $post = Post::create($data);
//        $post->tags()->sync(request('tags'));

        return $post;
    }

    public static function updatePost(PostRequest $request, $id)
    {
        $data = $request->all();
        $data['cover'] = self::uploadFile($request->file('file'));
        $data['user_id'] = auth()->check() ? auth()->user()->id : '';

        $post = Post::find($id);
        $post->update($data);
        $post->tags()->sync(request('tags'));

        return $post;
    }

    public static function uploadFile($file)
    {
        $imageName = '';
        if ($file != null) {
            $image_name = $file->getClientOriginalName();
            $name = time() . "" . $image_name;
            $image_public_path = public_path("images");
            $file->move($image_public_path, $name);
            $imageName = "images/" . $name;
        }
        return $imageName;
    }
}
