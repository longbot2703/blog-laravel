<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    protected $user, $role;

    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    public function index()
    {
        $lsUser = User::all();

        return view('user.list',compact('lsUser'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $allRole = Role::all();
        return view('user.edit',compact('allRole','user'));
    }

    public function update(UserRequest $request, $id)
    {
        $this->user->updateUser($request, $id);
        return redirect()->route("user_management.index")->with('success','User was updated!');
    }

    public function destroy(Request $request)
    {
        try {
            $user = User::find($request->id);

            if ($user != null) {
                $user->delete();
                return response()->json(['status' => 1, 'message' => 'Deleted']);
            } else {
                return response()->json(['status' => 0, 'message' => 'Does not exit']);
            }
        } catch (\Exception $e) {
            $e->getMessage();
            return response()->json(['status' => 0, 'message' => 'Error']);
        }
    }
}
