<?php

namespace App\Repositories;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Carbon\Carbon;

class CategoryRepository extends EloquentRepository
{
    public function model()
    {
        return Category::class;
    }

    public static function createCategory(CategoryRequest $request)
    {
        $cate = new Category();
        $cate->name = $request->name;
        $cate->created_at = Carbon::now();
        $cate->save();

        return $cate;
    }

    public static function updateCategory(CategoryRequest $request, $id)
    {
        $cate = Category::find($id);
        $cate->name = $request->name;
        $cate->save();

        return $cate;
    }
}

